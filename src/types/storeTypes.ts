import CalculationMethod from "../consts/CalculationMethod";

export interface MainState {
  calculationMethod: CalculationMethod;
  includeTax: boolean;
  salary: number;
}
