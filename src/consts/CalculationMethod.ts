enum CalculationMethod {
  month = 1,
  minPay = 2,
  day = 3,
  hour = 4,
}

export default CalculationMethod;
