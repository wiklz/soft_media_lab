import CalculationMethod from "./CalculationMethod";

const RadioInputsData: {
  label: string;
  value: CalculationMethod;
}[] = [
  { label: "Оклад за месяц", value: CalculationMethod.month },
  { label: "МРОТ", value: CalculationMethod.minPay },
  { label: "Оплата за день", value: CalculationMethod.day },
  { label: "Оплата за час", value: CalculationMethod.hour },
];

export default RadioInputsData;
