import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "./store";
import CalculationMethod from "./consts/CalculationMethod";
import UserInputData from "./Components/UserInputData";
import CalculationsResult from "./Components/CalculationsResult";
import SvgSprite from "./Components/UI/SvgSprite";

const App: React.FC = () => {
  const showResults = useSelector(
    (state: RootState) =>
      state.main.calculationMethod === CalculationMethod.month &&
      state.main.salary
  );

  return (
    <>
      <SvgSprite />
      <UserInputData />
      {showResults ? <CalculationsResult /> : null}
    </>
  );
};

export default App;
