const reverseStr = (str: string) => str.split("").reverse().join("");

const splitStr = (str: string) =>
  reverseStr(
    reverseStr(str)
      .match(/[0-9]{1,3}/g)
      ?.join(" ") || ""
  );

export default splitStr;
