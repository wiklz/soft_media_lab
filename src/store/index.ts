import {
  combineReducers,
  configureStore,
  getDefaultMiddleware,
} from "@reduxjs/toolkit";
import logger from "redux-logger";
import mainSlice from "./reducers/mainReducer";

const rootReducer = combineReducers({ main: mainSlice });

export type RootState = ReturnType<typeof rootReducer>;

export const store = configureStore({
  reducer: rootReducer,
  middleware: [...getDefaultMiddleware({ thunk: false }), ...[logger]],
  devTools: process.env.NODE_ENV !== "production",
});

export default rootReducer;
