import { createSlice } from "@reduxjs/toolkit";
import CalculationMethod from "../../consts/CalculationMethod";
import { MainState } from "../../types/storeTypes";

const initialState: MainState = {
  calculationMethod: CalculationMethod.month,
  includeTax: false,
  salary: 0,
};

const mainSlice = createSlice({
  name: "main",
  initialState,
  reducers: {
    changeCalcMethod: (state, { payload }) => {
      state.calculationMethod = payload;
    },
    toggleIncludeTax: (state, { payload }) => {
      state.includeTax = payload;
    },
    changeSalary: (state, { payload }) => {
      state.salary = payload;
    },
  },
});
export default mainSlice.reducer;
export const { changeCalcMethod, toggleIncludeTax, changeSalary } =
  mainSlice.actions;
