import React from "react";
import classes from "./styles.module.scss";

interface Props {
  legend: string;
  children: any;
}

const Fieldset: React.FC<Props> = ({ legend, children }) => {
  return (
    <fieldset className={classes.wrapper}>
      <legend className={classes.legend}>{legend}</legend>
      {children}
    </fieldset>
  );
};

export default Fieldset;
