import React from "react";
import CalculationMethod from "../../../consts/CalculationMethod";
import classes from "./styles.module.scss";

interface Props {
  value: string;
  calculationMethod: CalculationMethod;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const TextInput: React.FC<Props> = ({ value, calculationMethod, onChange }) => {
  const getLabel = () => {
    switch (calculationMethod) {
      case CalculationMethod.month:
        return <span className={classes.label}>&#8381;</span>;
      case CalculationMethod.day:
        return <span className={classes.label}>&#8381; в день</span>;
      case CalculationMethod.hour:
        return <span className={classes.label}>&#8381; в час</span>;

      default:
        return null;
    }
  };
  return (
    <label className={classes.wrapper}>
      <input
        className={classes.input}
        type="text"
        value={value}
        onChange={onChange}
      />
      {getLabel()}
    </label>
  );
};

export default TextInput;
