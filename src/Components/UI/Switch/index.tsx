import React, { useMemo } from "react";
import classes from "./styles.module.scss";

interface Props {
  label: string;
  checked: boolean;
  checkedDescription: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const Switch: React.FC<Props> = ({
  label,
  checked,
  checkedDescription,
  onChange,
}) => {
  const labelTextAppliedClasses = useMemo(() => {
    const styles = [classes.labelText];
    if (checked) {
      styles.push(classes.active);
    } else {
      styles.push(classes.inactive);
    }
    return styles.join(" ");
  }, [checked]);

  const checkedDescriptionAppliedClasses = useMemo(() => {
    const styles = [classes.checkedDescription];
    if (checked) {
      styles.push(classes.inactive);
    } else {
      styles.push(classes.active);
    }
    return styles.join(" ");
  }, [checked]);

  return (
    <div className={classes.wrapper}>
      <label className={classes.inputWrapper}>
        <span className={labelTextAppliedClasses}>{label}</span>
        <input
          className={classes.input}
          type="checkbox"
          name="withTax"
          checked={checked}
          onChange={onChange}
        />
        <span className={checkedDescriptionAppliedClasses}>
          {checkedDescription}
        </span>
      </label>
    </div>
  );
};

export default Switch;
