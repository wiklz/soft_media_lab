import React from "react";
import CalculationMethod from "../../../consts/CalculationMethod";
import Tooltip from "../Tooltip";
import classes from "./styles.module.scss";

interface Props {
  checked: boolean;
  withTooltip?: boolean;
  label: string;
  name: string;
  value: CalculationMethod;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const Radio: React.FC<Props> = ({
  label,
  value,
  name,
  checked,
  onChange,
  withTooltip = false,
}) => {
  return (
    <label className={classes.wrapper}>
      <input
        className={classes.input}
        type="radio"
        name={name}
        value={value}
        checked={checked}
        onChange={onChange}
      />
      <span className={classes.labelText}>{label}</span>
      {withTooltip && <Tooltip />}
    </label>
  );
};

export default Radio;
