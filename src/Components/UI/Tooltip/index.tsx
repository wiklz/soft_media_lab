import React, { useMemo, useState } from "react";
import classes from "./styles.module.scss";

const Tooltip: React.FC = () => {
  const [showTooltip, setShowTooltip] = useState(false);

  const toggleTooltip = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();

    setShowTooltip((prev) => !prev);
  };

  const textClasses = useMemo(() => {
    if (showTooltip) {
      return `${classes.text} ${classes.visible}`;
    }
    return classes.text;
  }, [showTooltip]);

  return (
    <div className={classes.wrapper}>
      <button className={classes.btn} onClick={toggleTooltip}>
        <svg width="20" height="20" viewBox="0 0 20 20">
          <use xlinkHref={`#svg-${showTooltip ? "cancel" : "info"}`} />
        </svg>
      </button>
      <p className={textClasses}>
        МРОТ - минимальный размер оплаты труда. Разный для Разных регионов.
      </p>
    </div>
  );
};

export default Tooltip;
