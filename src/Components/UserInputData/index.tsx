import React, { useCallback, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../store";
import {
  changeCalcMethod,
  toggleIncludeTax,
  changeSalary,
} from "../../store/reducers/mainReducer";
import RadioInputsData from "../../consts/RadioInputsData";
import Fieldset from "..//UI/Fieldset";
import Radio from "../UI/Radio";
import Switch from "../UI/Switch";
import TextInput from "../UI/TextInput";
import CalculationMethod from "../../consts/CalculationMethod";
import splitStr from "../../utils/splitStr";

const UserInputData: React.FC = () => {
  const dispatch = useDispatch();

  const calculationMethod = useSelector(
    (state: RootState) => state.main.calculationMethod
  );
  const includeTax = useSelector((state: RootState) => state.main.includeTax);
  const salary = useSelector((state: RootState) => state.main.salary);

  const onCalcMethodChange = useCallback(
    (e) => dispatch(changeCalcMethod(parseInt(e.target.value))),
    [dispatch]
  );

  const toggleTax = useCallback(
    (e) => dispatch(toggleIncludeTax(!e.target.checked)),
    [dispatch]
  );

  const onChangeSalary = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const numValue = parseInt(e.target.value.replaceAll(" ", "")) || 0;
      dispatch(changeSalary(numValue));
    },
    [dispatch]
  );

  const salaryValue = useMemo(() => {
    if (!salary) {
      return "";
    }
    return splitStr(`${salary}`);
  }, [salary]);

  return (
    <Fieldset legend="Сумма">
      {RadioInputsData.map((el) => (
        <Radio
          key={el.label}
          label={el.label}
          name="calculationMethod"
          value={el.value}
          checked={calculationMethod === el.value}
          onChange={onCalcMethodChange}
          withTooltip={CalculationMethod.minPay === el.value}
        />
      ))}
      {calculationMethod !== CalculationMethod.minPay && (
        <>
          <Switch
            label="Указать с НДФЛ"
            checked={!includeTax}
            checkedDescription="Без НДФЛ"
            onChange={toggleTax}
          />
          <TextInput
            onChange={onChangeSalary}
            value={salaryValue}
            calculationMethod={calculationMethod}
          />
        </>
      )}
    </Fieldset>
  );
};

export default UserInputData;
