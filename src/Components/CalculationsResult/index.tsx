import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../store";
import splitStr from "../../utils/splitStr";
import classes from "./styles.module.scss";

const CalculationsResult: React.FC = () => {
  const salaryNet = useSelector((state: RootState) => {
    const withTaxSubstracted = state.main.salary * 0.87;
    return state.main.includeTax ? withTaxSubstracted : state.main.salary;
  });

  const salaryBrut = (salaryNet / 87) * 100;

  const tax = salaryBrut * 0.13;

  return (
    <div className={classes.wrapper}>
      <p className={classes.text}>
        <span className={classes.price}>
          {splitStr(salaryNet.toFixed(0))} &#8381;
        </span>{" "}
        сотрудник будет получать на руки
      </p>
      <p className={classes.text}>
        <span className={classes.price}>
          {splitStr(tax.toFixed(0))} &#8381;
        </span>{" "}
        НДФЛ, 13% от оклада
      </p>
      <p className={classes.text}>
        <span className={classes.price}>
          {splitStr(salaryBrut.toFixed(0))} &#8381;
        </span>{" "}
        за сотрудника в месяц
      </p>
    </div>
  );
};

export default CalculationsResult;
