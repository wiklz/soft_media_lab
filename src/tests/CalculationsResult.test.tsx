import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../store";
import CalculationsResult from "../Components/CalculationsResult";

test("it renders 3 rows of prices", () => {
  render(
    <Provider store={store}>
      <CalculationsResult />
    </Provider>
  );

  const texts = screen.getAllByText(/₽/);

  expect(texts).toHaveLength(3);
});
