import { render, screen } from "@testing-library/react";
import FieldSet from "../Components/UI/Fieldset";

const mockProps = {
  legend: "legend",
  children: null,
};

test("it renders a fildset with correct legend", () => {
  render(<FieldSet {...mockProps} />);

  const fieldset = screen.getByRole("group", { name: "legend" });

  expect(fieldset).toBeInTheDocument();
});
