import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Tooltip from "../Components/UI/Tooltip";

test("initially doesn't show tooltip text", () => {
  render(<Tooltip />);

  const textElement = screen.getByText(
    "МРОТ - минимальный размер оплаты труда. Разный для Разных регионов."
  );

  expect(textElement).not.toHaveClass("visible");
});

test("shows tooltip text on button click", () => {
  render(<Tooltip />);

  const textElement = screen.getByText(
    /МРОТ - минимальный размер оплаты труда. Разный для Разных регионов./
  );

  const btnElement = screen.getByRole("button");

  userEvent.click(btnElement);

  expect(textElement).toHaveClass("visible");
});
