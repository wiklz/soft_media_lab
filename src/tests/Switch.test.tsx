import { render, screen } from "@testing-library/react";
import Switch from "../Components/UI/Switch";

const mockProps = {
  label: "label",
  checked: true,
  checkedDescription: "description",
  onChange: jest.fn(),
};

test("it renders text withoutTax with correct classes", () => {
  render(<Switch {...mockProps} />);

  const withoutTaxText = screen.getByText("label");

  expect(withoutTaxText).toHaveClass("labelText active");
});

test("it renders text withTax with correct classes", () => {
  render(<Switch {...mockProps} />);

  const withoutTaxText = screen.getByText("description");

  expect(withoutTaxText).toHaveClass("checkedDescription inactive");
});
