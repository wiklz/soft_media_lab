import { render, screen } from "@testing-library/react";
import Radio from "../Components/UI/Radio";

const mockProps = {
  checked: false,
  label: "label",
  name: "name",
  value: 1,
  onChange: jest.fn(),
};

test("it renders a radio button with correct label", () => {
  render(<Radio {...mockProps} />);

  const radio = screen.getByRole("radio", { name: "label" });

  expect(radio).toBeInTheDocument();
});
