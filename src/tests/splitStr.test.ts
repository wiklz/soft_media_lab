import splitStr from "../utils/splitStr";

test("it returns correct values", () => {
  const cases = {
    case1: splitStr(""),
    case2: splitStr("abc"),
    case3: splitStr("1234abc"),
    case4: splitStr("1234567890"),
  };

  expect(cases.case1).toBe("");
  expect(cases.case2).toBe("");
  expect(cases.case3).toBe("1 234");
  expect(cases.case4).toBe("1 234 567 890");
});
