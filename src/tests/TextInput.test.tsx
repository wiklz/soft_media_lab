import { render, screen } from "@testing-library/react";
import TextInput from "../Components/UI/TextInput";

test("it renders a text input with correct label", () => {
  render(<TextInput value="" calculationMethod={3} onChange={jest.fn()} />);

  const input = screen.getByRole("textbox", { name: "₽ в день" });

  expect(input).toBeInTheDocument();
});
